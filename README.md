# Newsapp #

A mobile app that shows some news headlines. An onboarding project for Icehouse internship.

### Features ###

* Login page
* Homescreen with headlines

### Dependencies ###

* SwiftUI
* Moya
* CoreStore